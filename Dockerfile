FROM openjdk:8-jdk-alpine
ADD https://bitbucket.org/bigyan121/java-spring/raw/d54e9eea7af3579df08c0c1393f68a877f1ccb20/target/demo-0.0.1-SNAPSHOT.jar /opt/spring-boot.jar
EXPOSE 80/tcp
CMD ["/bin/sh", "ls", "/opt"]
ENTRYPOINT ["java", "-jar", "/opt/spring-boot.jar"]